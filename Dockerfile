FROM jetbrains/teamcity-agent:latest

ENV PACKER_VERSION=0.12.2
	  
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF \
	&& apt-get -y --no-install-recommends install curl apt-transport-https ca-certificates \
	&& echo "deb https://download.mono-project.com/repo/ubuntu stable-xenial main" | tee /etc/apt/sources.list.d/mono-official-stable.list \
	&& apt update \
	&& apt-get -y --no-install-recommends install ca-certificates software-properties-common mono-devel

# Install Node.js
RUN curl --silent --location https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install --yes nodejs build-essential

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
		awscli

#powershell
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - \
	&& curl https://packages.microsoft.com/config/ubuntu/16.04/prod.list | tee /etc/apt/sources.list.d/microsoft.list \
	&& apt-get update \
	&& apt-get install -y powershell\
	&& apt-get clean \
    && rm -rf /var/lib/apt/lists/*
		
# Setup docker-credential-ecr-login
COPY config.json /root/.docker/config.json
RUN chmod 0644 /root/.docker/config.json

COPY config.json /home/buildagent/.docker/config.json
RUN chmod 0644 /home/buildagent/.docker/config.json

COPY bin/docker-credential-ecr-login /usr/bin/docker-credential-ecr-login
RUN chmod 0755 /usr/bin/docker-credential-ecr-login

# Setup docker in docker file system driver to overlay2
COPY daemon.json /etc/docker/daemon.json
RUN chmod 0644 /etc/docker/daemon.json

